import "../styles/base.scss";

import * as jQuery from "jquery";



jQuery(() => {
    jQuery('#contact-form').submit(validateForm);
});

function renderErrors(errors) {
    const $errors = jQuery("#errors");

    $errors.empty();
    $errors.show();

    for(let error of errors) {
        $errors.append("<div> &raquo; " + error + "</div>");
    }

}

function validateForm(e) {
    e.preventDefault();

    let errors = [];

    const $form = jQuery(e.target);

    $form.find('[data-required=1]').each((i, el) => {
        const $e = jQuery(el);

        const $label = $e.find('label');
        const forId = $label.attr('for');

        if (!forId) return;

        let title = $label.text();

        if(title.substr(-1) === ':') {
            title = title.substr(0, title.length - 1);
        }

        const val = jQuery(`#${forId}`).val().trim();

        if (val === '') {
            errors.push(`Lauks "${title}" ir jānorāda obligāti!`);
        }
    });

    if (errors.length) {
        renderErrors(errors);
    } else {
        $form.get(0).submit();
    }
}
