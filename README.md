# Usage
Either build or use prebuilt sources

## Launching
This repository already includes prebuilt sources, just open "dist/index.html" in your browser.

## Building
1. Make sure you have NodeJS 8+ installed and run:  ```npm i```
2. Launch Dev server ```npm run server```
3. Open http://localhost:9000 in your browser
